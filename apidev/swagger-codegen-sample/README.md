
# Use OpenAPI(Swagger) tools to develop restful APIs:

## Install OpenAPI (swagger) editor extension in VS : <br>  

Follow the instrcution on the link: https://marketplace.visualstudio.com/items?itemName=42Crunch.vscode-openapi

##	Install  swagger-codegen-cli: <br>

On Linux:  <br>

Download current stable 2.x.x branch (Swagger and OpenAPI version 2) <br>
wget https://repo1.maven.org/maven2/io/swagger/swagger-codegen-cli/2.4.23/swagger-codegen-cli-2.4.23.jar -O swagger-codegen-cli.jar


Or  to download current stable 3.x.x branch (OpenAPI version 3)<br>
wget https://repo1.maven.org/maven2/io/swagger/codegen/v3/swagger-codegen-cli/3.0.29/swagger-codegen-cli-3.0.29.jar -O swagger-codegen-cli.jar


On Windows: <br>
Install wget or use Invoke-WebRequest in PowerShell (3.0+), e.g. <br>

Invoke-WebRequest -OutFile swagger-codegen-cli.jar https://repo1.maven.org/maven2/io/swagger/swagger-codegen-cli/2.4.23/swagger-codegen-cli-2.4.23.jar




## Process to develop RESTAPI:
- Create OpenAPI (swagger) document using  OpenAPI editor in VS code, save to ./swagger.yaml

- Generate server side code stub:

java -jar d:\tools\swagger-codegen\swagger-codegen-cli.jar generate -i ./swagger.yaml -l nodejs-server -o .\generated\nodejs-server  <br>


- Generate api-client code stub:

java -jar d:\tools\swagger-codegen\swagger-codegen-cli.jar generate -i ./swagger.yaml -l android -o .\generated\android

java -jar d:\tools\swagger-codegen\swagger-codegen-cli.jar generate -i ./swagger.yaml -l swift -o .\generated\swift

