https://scotch.io/tutorials/speed-up-your-restful-api-development-in-node-js-with-swagger

Installation and run the example

1. install swagger:
$npm install -g swagger

2. create project with swagger enabled:
$swagger project create movie-collection
and choose the "express framework"

3. Swagger editor is an elegant browser-based editor which really simplifies our efforts to develop a web API. In particular, it provides:

    Validation and endpoint routing.
    Docs on the fly generation.
    Easy-to-read Yaml.



4. launch the sample app
$swagger project start    #whenever we modify a file, it automatically restarts the server

5. launch swagger editor:
$swagger project edit
