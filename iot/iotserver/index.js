// Enrich data set

/*
* Copyright 2010-2019 Amazon.com, Inc. or its affiliates. All Rights Reserved.
*
* Licensed under the Apache License, Version 2.0 (the "License").
* You may not use this file except in compliance with the License.
* A copy of the License is located at
*
*  http://aws.amazon.com/apache2.0
*
* or in the "license" file accompanying this file. This file is distributed
* on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied. See the License for the specific language governing
* permissions and limitations under the License.
*/

// Requires
const weather = require('weather-js');
const gps = require('gps2zip'); 

// Handler called by IoT Analytics
exports.handler = async(event) => {

    console.log(JSON.stringify(event));
    
    // Lookup the zip code based on latitude and longitude
    var zipLookupResponse = gps.gps2zip(event.latitude, event.longitude);
    
    console.log(JSON.stringify(zipLookupResponse));
    
    // Lookup the weather based on the zip code
    weather.find({search: zipLookupResponse.zip_code, degreeType: 'F'}, function(err, result) {
        // If there is an error
        if(err) {
            console.log(err);
            return JSON.stringify(err);
        }
        
        // Add the weather and zipcode to the input payload
        event.weather = result[0].current.skytext;
        event.zipcode = zipLookupResponse.zip_code;
        //event.address = zipLookupResponse.address;
        
        // Return the data        
        return JSON.stringify(event);
    });

    //return JSON.stringify(event);
};
